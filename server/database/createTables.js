// const connection = require("./dbConnection");
const { CREATE_USER_TABLE } = require("./createTableQueries/users");

const createTableList = [CREATE_USER_TABLE];

module.exports.createTables = (connection) => {
  createTableList.forEach((elm) => {
    connection.query(elm, async (error, results, fields) => {
      if (error) return console.log(error);
    });
  });
};
