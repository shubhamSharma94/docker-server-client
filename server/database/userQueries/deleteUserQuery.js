const DELETE_ALL_USER = () => {
  return `
        UPDATE users
            SET active = 0
            WHERE active = 1;
      `;
};

const DELETE_USER_BY_ID = (id) => {
  return `
        UPDATE users
            SET active = 0
            WHERE id = ${id};
      `;
};

module.exports.DELETE_ALL_USER = DELETE_ALL_USER;
module.exports.DELETE_USER_BY_ID = DELETE_USER_BY_ID;
