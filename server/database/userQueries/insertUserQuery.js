const moment = require("moment");

const INSERT_USER = ({
  name,
  gender,
  email,
  dob,
  phone,
  cell,
  address,
  pictureThumbnail,
  pictureMedium,
  pictureLarge,
}) => {
  return `
        INSERT INTO users
        (name,
        gender,
        email,
        dob,
        phone,
        cell,
        address,
        picture_thumbnail,
        picture_medium,
        picture_large
        )
        VALUES
        (
        "${name}",
        "${gender}",
        "${email}",
        "${moment(dob).format("MM-DD-YYYY")}",
        "${phone}",
        "${cell}",
        "${address}",
        "${pictureThumbnail}",
        "${pictureMedium}",
        "${pictureLarge}"
        );
    `;
};

module.exports.INSERT_USER = INSERT_USER;
