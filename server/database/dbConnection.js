require("dotenv").config();

var mysql = require("mysql");

const db_config = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_SCHEMA,
  connectionLimit: 10,
};

var connection;

function handleDisconnect() {
  connection = mysql.createPool(db_config); // Recreate the connection, since
  // the old one cannot be reused.
  connection.on("error", function (err) {
    console.log("db error", err);
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();

connection.query(
  `USE ${db_config.database};`,
  async (error, results, fields) => {
    if (error) console.log(error);
  }
);

const createTables = require("./createTables").createTables(connection);

module.exports.connection = connection;
