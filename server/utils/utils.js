const buildUserObject = (data) => {
  let {
    name,
    gender,
    email,
    dob,
    phone,
    cell,
    location,
    picture,
  } = data.results[0];

  let newUserObjectForDB = {
    name: `${name.title} ${name.first} ${name.last}`,
    gender: gender,
    email: email,
    dob: dob.date,
    phone: phone,
    cell: cell,
    address: buildAddress(location),
    pictureThumbnail: picture.thumbnail,
    pictureMedium: picture.medium,
    pictureLarge: picture.large,
  };

  return newUserObjectForDB;
};

const buildAddress = ({ street, city, state, country, postcode }) => {
  return `${street.number}, ${street.name}, ${city}, ${state}, ${country} - ${postcode}`;
};

module.exports.buildUserObject = buildUserObject;
