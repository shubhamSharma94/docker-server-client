const axios = require("axios");

module.exports.createUser = () => {
  return new Promise((resolve, reject) => {
    return axios
      .get("https://randomuser.me/api/")
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject({});
      });
  });
};
