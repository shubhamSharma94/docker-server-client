const user = require("./user");

const routesPackage = [user];

const setRoute = (appInstance) => {
  routesPackage.forEach((func) => {
    func(appInstance);
  });
};

module.exports = (app) => {
  setRoute(app);
};
