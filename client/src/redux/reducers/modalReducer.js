import { SHOW_MODAL, HIDE_MODAL } from "../constants/modalConstants";

const initialData = {
  modalData: {},
  showModal: false,
};

export const modalReducer = (state = initialData, action = {}) => {
  switch (action.type) {
    case SHOW_MODAL:
      return Object.assign({}, state, {
        showModal: true,
        modalData: action.payload,
      });

    case HIDE_MODAL:
      return Object.assign({}, state, {
        showModal: false,
        modalData: {},
      });

    default:
      return state;
  }
};
