import axios from "axios";

let url = "";
if (process.env.NODE_ENV === "development") {
  url = "http://node-api-server:4000";
}

if (process.env.NODE_ENV === "production") {
  url = "/";
}

export const apiHandler = axios.create({ baseURL: url });
