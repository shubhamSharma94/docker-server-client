import Layout from "antd/lib/layout";
import ModalComponent from "./components/molecule/UserList/ModalComponent/ModalComponent";
import UserContainer from "./components/organism/UserContainer";

const { Header, Footer, Content } = Layout;

function App() {
  return (
    <div className='App'>
      <Layout>
        <Header>KaamWork</Header>
        <Content>
          <UserContainer />
          <ModalComponent />
        </Content>
        <Footer>&#169; KaamWork</Footer>
      </Layout>
    </div>
  );
}

export default App;
