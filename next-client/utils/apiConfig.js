import axios from "axios";

let url = "";
if (process.env.NODE_ENV === "development") {
  url = "http://localhost:4000";
}

if (process.env.NODE_ENV === "production") {
  url = "https://kaam-work-assingment-server.herokuapp.com/";
}

export const apiHandler = axios.create({ baseURL: url });
