import Layout from "antd/lib/layout";
import ModalComponent from "../components/molecule/UserList/ModalComponent/ModalComponent";
import UserContainer from "../components/organism/UserContainer";
import configureStore from "../redux/store/configureStore";
import { Provider } from "react-redux";

const store = configureStore();

const { Header, Footer, Content } = Layout;

export default function Home() {
  return (
    <Provider store={store}>
      <Layout>
        <Header>KaamWork</Header>
        <Content>
          <UserContainer />
          <ModalComponent />
        </Content>
        <Footer>&#169; KaamWork</Footer>
      </Layout>
    </Provider>
  );
}
